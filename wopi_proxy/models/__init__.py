# SPDX-FileCopyrightText: 2020 Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2


from .edit_request import EditRequest

__all__ = ["EditRequest"]

# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

__version__ = "0.0.3"
import minty_pyramid
from .routes import routes


def main(*args, **kwargs):
    loader = minty_pyramid.GenericEngine()
    config = loader.setup(*args, **kwargs)
    routes.add_routes(config)
    return loader.main()

# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import datetime
import json
import logging
import os
import re
import requests
import secrets
import xml.etree.ElementTree as ET
from pydantic import ValidationError as pydanticValidationError
from pyramid import httpexceptions as exec
from pyramid.response import Response
from redis import Redis
from wopi_proxy.models import EditRequest

logger = logging.getLogger(__name__)

infra = {"redis": None}


def _intialize_redis(host, port, password=None, db=0):
    """Initialze Redis for WOPI"""
    if infra["redis"] is None:
        infra["redis"] = Redis(host=host, port=port, password=password, db=db)


def _set_redis(name, value, ex):
    """Set value in redis"""
    infra["redis"].set(name, value, ex)


def _get_redis(name):
    """Get value from redis"""
    return infra["redis"].get(name)


def _remove_redis(name):
    """Remove value from redis"""
    return infra["redis"].delete(name)


def _get_ttl(name):
    """Get ttl for redis key"""
    return infra["redis"].ttl(name)


def _get_discovery_xml(discovery_url):
    """Get discovery xml from discovery_url"""
    try:
        discovery_xml = requests.request("GET", discovery_url).text
        # Caching discovery_xml for one day in Redis.
        _set_redis(
            name="wopi_service:discovery_xml", value=discovery_xml, ex=86400
        )
    except Exception as e:
        raise exec.HTTPConflict(json={"errors": [{"title": f"{e}"}]})
    return discovery_xml


def _format_urlsrc(urlsrc, replacements):
    """Replace known placeholders with placeholder values and remove unknown placeholders along with angle brackets"""

    def _template_replacer(match: re.Match):
        template_variable = match.group(1)
        template_replaced = template_variable
        for k, v in replacements.items():
            template_replaced = template_replaced.replace(k, v)

        if template_replaced == template_variable:
            # Nothing was filled in; remove the template variable
            return ""

        return template_replaced

    return re.sub(r"<(.*?)>", _template_replacer, urlsrc)


def _get_wopi_urls(
    discovery_url, app, action, extension, wopi_source, business_user
):
    """Get urlsrc and favIconUrl from discovery_xml of MS WOPI"""

    discovery_xml = _get_redis(name="wopi_service:discovery_xml")
    if discovery_xml is None:
        discovery_xml = _get_discovery_xml(discovery_url=discovery_url)

    # Parsing discovery_xml
    root = ET.fromstring(discovery_xml)

    xml_app = root.find(f".//app[@name='{app}']")
    if not xml_app:
        raise exec.HTTPNotFound(
            json={
                "errors": [{"title": f"No app '{app}' found in discovery_xml"}]
            }
        )

    favIconUrl = xml_app.get("favIconUrl")
    if not favIconUrl:
        raise exec.HTTPNotFound(
            json={
                "errors": [
                    {
                        "title": f"No favIconUrl found in discovery_xml for app '{app}'"
                    }
                ]
            }
        )

    try:
        urlsrc = xml_app.find(
            f"./action[@ext='{extension}'][@name='{action}']"
        ).get("urlsrc")
    except AttributeError:
        urlsrc = None

    if not urlsrc:
        raise exec.HTTPNotFound(
            json={
                "errors": [
                    {
                        "title": f"No urlsrc found in discovery_xml for app '{app}' with action '{action}'"
                    }
                ]
            }
        )

    replacements = {
        "WOPI_SOURCE": wopi_source,
        "BUSINESS_USER": "1" if business_user else "0",
        "DISABLE_CHAT": "1",
    }

    urlsrc = _format_urlsrc(urlsrc=urlsrc, replacements=replacements)

    return favIconUrl, urlsrc


def edit_document(request):
    """Generate a link to start the MS WOPI editor"""
    file_uuid = request.json_body["document_uuid"]
    logger = logging.getLogger(__name__)
    logger.info(
        f"Called edit_document on file with uuid '{file_uuid}' with json_body {request.json_body}"
    )

    redis_configuration = request.configuration["redis"]
    wopi_configuration = request.configuration["wopi-proxy"]
    host_url = "https://" + request.host

    _intialize_redis(**redis_configuration["session"])

    try:
        request = EditRequest(**request.json_body)
    except pydanticValidationError as e:
        logger.warning(f"Caught exception: {e}", exc_info=True)
        raise exec.HTTPBadRequest(json={"errors": [{"title": str(e)}]})

    api_key = wopi_configuration["api_key"]
    if api_key != request.api_key:
        raise exec.HTTPUnauthorized(
            json={
                "errors": [
                    {"title": "Authentication failed. Invalid api key."}
                ]
            }
        )

    discovery_url = wopi_configuration["discovery_url"]
    document_uuid = str(file_uuid)

    wopi_source = host_url + f"/wopi/files/{document_uuid}"
    favIconUrl, urlsrc = _get_wopi_urls(
        discovery_url,
        request.app,
        request.action,
        request.file_info.extension,
        wopi_source,
        request.business_user,
    )

    # An access token is a string used by the host to determine
    # the identity and permissions of the issuer of a WOPI request.
    access_token = secrets.token_urlsafe()

    # The access_token_ttl property tells a WOPI client when an access token expires,
    # represented as the number of milliseconds since January 1, 1970 UTC (the date epoch)
    ten_hours_from_now = datetime.datetime.now(
        tz=datetime.timezone.utc
    ) + datetime.timedelta(hours=10)
    access_token_ttl = int(ten_hours_from_now.timestamp() * 1000.0)

    access_token_data = {
        "context": request.context,
        "document_uuid": request.document_uuid,
        "action": request.action,
        "user_uuid": request.user_uuid,
        "user_display_name": request.user_display_name,
        "save_url": request.callback_settings.save_url,
        "save_url_params": request.callback_settings.save_url_params,
        "close_url": request.callback_settings.close_url,
        "host_edit_url": request.callback_settings.host_page_url,
    }

    # Cache the document_data in redis for 10 hours (ie until the access_token expires)
    # if the document is already in redis, override it with new latest document_data
    document_data = {
        "document_uuid": request.document_uuid,
        "extension": request.file_info.extension,
        "size": request.file_info.size,
        "filename": request.file_info.filename,
        "version": str(request.file_info.version),
        "file_url": request.file_info.file_url,
        "owner_uuid": request.owner_uuid,
    }
    _set_redis(
        name=f"wopi_service:document:{document_uuid}",
        value=json.dumps(document_data),
        ex=36000,
    )

    # Caching the user_data in Redis for 10 hours.
    _set_redis(
        name=f"wopi_service:{access_token}",
        value=json.dumps(access_token_data),
        ex=36000,
    )

    logger.info(f"edit_document success for file with uuid {document_uuid}")
    return {
        "favIconUrl": favIconUrl,
        "urlsrc": urlsrc,
        "access_token": access_token,
        "access_token_ttl": access_token_ttl,
    }


def check_file_info(request):
    """The CheckFileInfo operation returns information about a file,
    a user’s permissions on that file, and general information about
    the capabilities that the WOPI host has on the file"""

    request_params = _validate_wopi_request(request)
    file_uuid = request_params["file_uuid"]

    logger = logging.getLogger(__name__)
    logger.info(f"check_file_info called on file with uuid '{file_uuid}'")

    wopi_configuration = request.configuration["wopi-proxy"]
    dev_mode = wopi_configuration.get("dev_mode", "false")

    access_token_data = _validate_access_token(
        request_params["access_token"], file_uuid
    )
    document_data = _get_document_data(file_uuid)

    file_info = {
        "BaseFileName": document_data["filename"],
        "OwnerId": document_data["owner_uuid"],
        "Size": document_data["size"],
        "UserId": access_token_data["user_uuid"],
        "UserFriendlyName": access_token_data["user_display_name"],
        "Version": document_data["version"],
        "SupportsUpdate": True,
        "UserCanWrite": True,
        "FileUrl": document_data["file_url"],
        "SupportsExtendedLockLength": True,
        "PostMessageOrigin": access_token_data["context"],
        "SupportsLocks": True,
        "SupportsGetLock": True,
        "EditModePostMessage": True,
        "SupportsCobalt": False,
        "ClosePostMessage": True,
        "CloseUrl": access_token_data["close_url"],
        "UserCanNotWriteRelative": True,
        "LicenseCheckForEditIsEnabled": True,
        "HostEditUrl": access_token_data["host_edit_url"],
    }

    if dev_mode == "true":
        del file_info["FileUrl"]

    if document_data["extension"] == "odt":
        current_lock = _get_redis(f"wopi_service:{file_uuid}:lock")
        if current_lock:
            file_info["ReadOnly"] = True

    logger.info(
        f"check_file_info success for file with uuid {file_uuid} with response {file_info}"
    )

    return file_info


def _validate_access_token(access_token, file_uuid):
    try:
        redis_key = f"wopi_service:{access_token}"
        access_token_data = _get_redis(redis_key)
        access_token_data = json.loads(access_token_data)
    except (KeyError, TypeError):
        raise exec.HTTPUnauthorized(
            json={"errors": [{"title": "Invalid access token"}]}
        )

    if file_uuid and file_uuid != access_token_data["document_uuid"]:
        raise exec.HTTPUnauthorized(
            json={"errors": [{"title": "Invalid access token"}]}
        )
    return access_token_data


def _validate_wopi_request(request):
    try:
        file_uuid = request.matchdict["file_id"]
        access_token = request.params["access_token"]
    except KeyError as e:
        raise exec.HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter {e}"}]}
        )

    x_wopi_override = request.headers.get("x_wopi_override", "")
    x_wopi_lock = request.headers.get("x_wopi_lock", "")
    x_wopi_oldlock = request.headers.get("x_wopi_oldlock", "")
    file = request.body_file

    if x_wopi_override:
        if x_wopi_override == "PUT_RELATIVE":
            raise exec.HTTPNotImplemented(
                json={
                    "errors": [
                        {"title": "PutRelativeFile is not Implemeneted"}
                    ]
                }
            )

        if x_wopi_override not in [
            "LOCK",
            "UNLOCK",
            "REFRESH_LOCK",
            "GET_LOCK",
            "PUT",
        ]:
            raise exec.HTTPBadRequest(
                json={"errors": [{"title": "Invalid X-WOPI-Override"}]}
            )

    if (
        x_wopi_override in ["LOCK", "UNLOCK", "REFRESH_LOCK"]
        and not x_wopi_lock
    ):
        raise exec.HTTPBadRequest(
            json={"errors": [{"title": "X-WOPI-Lock is not provided"}]}
        )

    if x_wopi_oldlock and x_wopi_override == "LOCK":
        x_wopi_override = "UNLOCK_AND_RELOCK"

    return {
        "file_uuid": file_uuid,
        "access_token": access_token,
        "x_wopi_lock": x_wopi_lock,
        "x_wopi_oldlock": x_wopi_oldlock,
        "x_wopi_override": x_wopi_override,
        "file": file,
    }


def _validate_current_lock(current_lock):
    """Validate current lock on file. If the current_lock is longer that 1024 ASCII chars(maximum_lock_length)raise Conflict"""

    try:
        lock_ascii = current_lock.encode("us-ascii")
    except UnicodeEncodeError:
        # Not ASCII
        raise exec.HTTPConflict(
            json={
                "errors": [
                    {"title": "Invalid lock. Non-ASCII characters found"}
                ]
            },
            headers=[
                ("X-WOPI-Lock", ""),
                (
                    "X-WOPI-LockFailureReason",
                    "Lock has Non-ASCII characters",
                ),
            ],
        )

    if len(lock_ascii) > 1024:
        raise exec.HTTPConflict(
            json={
                "errors": [
                    {"title": "Lock is longer than 'maximum lock length'"}
                ]
            },
            headers=[
                ("X-WOPI-Lock", ""),
                (
                    "X-WOPI-LockFailureReason",
                    "Lock is longer than 'maximum lock length'",
                ),
            ],
        )


def _compare_locks(x_wopi_lock, current_lock):
    """Compare current_lock on file with the x_wopi_lock/x_wopi_old_lock given in request_header."""
    if current_lock != x_wopi_lock:
        raise exec.HTTPConflict(
            json={"errors": [{"title": "Lock mismatch"}]},
            headers=[
                ("X-WOPI-Lock", current_lock),
                (
                    "X-WOPI-LockFailureReason",
                    "Lock mismatch",
                ),
            ],
        )


def _get_document_data(document_uuid):
    document_data = _get_redis(f"wopi_service:document:{document_uuid}")
    return json.loads(document_data) if document_data else None


def lock_file(request):
    """The Lock operation locks a file for editing by the
    WOPI client application instance that requested the lock.
    """
    request_params = _validate_wopi_request(request)

    file_uuid = request_params["file_uuid"]
    x_wopi_override = request_params["x_wopi_override"]

    logger = logging.getLogger(__name__)
    logger.info(
        f"{x_wopi_override.lower()} called on file with uuid '{file_uuid}'"
    )
    _validate_access_token(
        request_params["access_token"], request_params["file_uuid"]
    )
    document_data = _get_document_data(request_params["file_uuid"])

    x_wopi_lock = request_params["x_wopi_lock"]
    x_wopi_oldlock = request_params["x_wopi_oldlock"]

    lock_redis_key = f"wopi_service:{file_uuid}:lock"
    current_lock = _get_redis(lock_redis_key) or b""
    current_lock = current_lock.decode("utf-8")

    if x_wopi_override == "GET_LOCK":
        _validate_current_lock(current_lock)
        logger.info(
            f"{x_wopi_override.lower()} success for file with uuid '{file_uuid}'"
        )
        return Response(
            body="success",
            status=200,
            charset="utf-8",
            headerlist=[("X-WOPI-Lock", str(current_lock))],
        )

    elif x_wopi_override == "LOCK":
        if current_lock:
            _compare_locks(x_wopi_lock, current_lock)
            _validate_current_lock(current_lock)

        _set_redis(
            name=lock_redis_key,
            value=x_wopi_lock,
            ex=1800,
        )

    elif x_wopi_override == "REFRESH_LOCK":
        _compare_locks(x_wopi_lock, current_lock)
        _validate_current_lock(current_lock)
        _set_redis(
            name=lock_redis_key,
            value=x_wopi_lock,
            ex=1800,
        )

    elif x_wopi_override == "UNLOCK":
        _compare_locks(x_wopi_lock, current_lock)
        _validate_current_lock(current_lock)
        _remove_redis(name=lock_redis_key)

    elif x_wopi_override == "UNLOCK_AND_RELOCK":
        _compare_locks(x_wopi_oldlock, current_lock)
        _validate_current_lock(current_lock)
        _set_redis(
            name=lock_redis_key,
            value=x_wopi_lock,
            ex=_get_ttl(lock_redis_key),
        )

    logger.info(
        f"{x_wopi_override.lower()} success for file with uuid '{file_uuid}'"
    )
    return Response(
        body="success",
        status=200,
        charset="utf-8",
        headerlist=[("X-WOPI-ItemVersion", document_data["version"])],
    )


def put_file(request):
    """Update the contents of a file in WOPI client."""
    request_params = _validate_wopi_request(request)
    file_uuid = request_params["file_uuid"]
    access_token = request_params["access_token"]

    logger = logging.getLogger(__name__)
    logger.info(f"put_file called on file with uuid '{file_uuid}'")

    access_token_data = _validate_access_token(access_token, file_uuid)

    document_data = _get_document_data(file_uuid)

    file = request_params["file"]
    x_wopi_lock = request_params["x_wopi_lock"]
    x_wopi_override = request_params["x_wopi_override"]

    if x_wopi_override != "PUT":
        raise exec.HTTPBadRequest(
            json={"errors": [{"title": "Invalid X-WOPI-Override"}]}
        )

    try:
        current_lock = _get_redis(f"wopi_service:{file_uuid}:lock").decode(
            "utf-8"
        )
    except AttributeError:
        current_lock = ""

    _compare_locks(x_wopi_lock, current_lock)
    _validate_current_lock(current_lock)

    if not current_lock and document_data.get("size", None) != 0:
        raise exec.HTTPConflict(
            json={
                "errors": [
                    {
                        "title": f"Unlocked file with UUID '{file_uuid}' is not empty."
                    }
                ]
            },
        )

    save_url = access_token_data["save_url"]
    # seeks to the start of the file.
    file.seek(0, os.SEEK_SET)
    save_data = {"content": (document_data["filename"], file.read())}
    for key, value in access_token_data["save_url_params"].items():
        save_data[key] = (None, value)

    # Call WOPI client save_url to put file content.
    save_file_response = requests.post(
        url=save_url,
        files=save_data,
        verify=False,
    )
    if save_file_response.status_code != 200:
        save_file_response.raise_for_status()

    # seeks to the end of the file, to get the file_size
    file.seek(0, os.SEEK_END)
    # Update the size of document in redis.
    document_data["size"] = file.tell()
    # seeks back to the start, so the next "read" from that file handle starts at the beginning of the file again
    file.seek(0, os.SEEK_SET)

    # Update version of document in redis.
    document_data["version"] = (
        str(save_file_response.json().get("new_version"))
        or document_data["version"]
    )
    # Update file_url of document in redis.
    document_data["file_url"] = (
        save_file_response.json().get("new_file_url")
        or document_data["file_url"]
    )

    redis_key = f"wopi_service:document:{file_uuid}"
    _set_redis(redis_key, json.dumps(document_data), _get_ttl(redis_key))

    logger.info(f"put_file success for file with uuid '{file_uuid}'")
    return Response(
        body="Success",
        status=200,
        charset="utf-8",
        headerlist=[("X-WOPI-ItemVersion", document_data["version"])],
    )


def get_file(request):
    """Get file contents"""
    request_params = _validate_wopi_request(request)

    file_uuid = request_params["file_uuid"]
    logger = logging.getLogger(__name__)
    logger.info(f"get_file called on file with uuid '{file_uuid}'")

    _validate_access_token(request_params["access_token"], file_uuid)
    document_data = _get_document_data(file_uuid)

    file_url = document_data["file_url"]
    res = requests.get(url=file_url, verify=False)
    if res.status_code != 200:
        res.raise_for_status()

    logger.info(f"get_file success for file with uuid '{file_uuid}'")
    return Response(
        body=res.content,
        status=200,
        charset="utf-8",
        headerlist=[("X-WOPI-ItemVersion", document_data["version"])],
    )


def health_check(request):
    try:
        _intialize_redis(**request.configuration["redis"]["session"])
        _get_discovery_xml(
            discovery_url=request.configuration["wopi-proxy"]["discovery_url"]
        )
    except KeyError as e:
        raise exec.HTTPInternalServerError(
            json={"errors": [{"title": f"Missing parameter {e}"}]}
        )
    except (exec.HTTPConflict, Exception) as e:
        logger = logging.getLogger(__name__)
        logger.warning(f"Caught exception: {e}", exc_info=True)
        raise exec.HTTPInternalServerError(
            json={"errors": [{"title": str(e)}]}
        )
    return Response(body="Success", status=200)

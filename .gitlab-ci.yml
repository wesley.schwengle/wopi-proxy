# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

.qa_rules: &qa_rules
    rules:
        - if: $CI_MERGE_REQUEST_TARGET_BRANCH_NAME
        - if: $CI_COMMIT_TAG =~ /^release\//
        - if: $CI_COMMIT_BRANCH =~ /^(master|production|preprod|development|experimental)$/

default:
    image: ${BUILDER_CONTAINER}:master

stages:
    - QA
    - Build
    - Release

REUSE Compliance:
    <<: *qa_rules
    stage: QA
    image:
        name: fsfe/reuse:latest
        entrypoint: [""]
    script:
        - reuse lint

OpenAPI Lint:
    <<: *qa_rules
    stage: QA
    image:
        name: stoplight/spectral
        entrypoint: [""]
    script:
        - spectral lint --fail-severity=warn apidocs/openapi30.json

Run Python tests:
    <<: *qa_rules
    stage: QA
    image:
        name: python:3.9-slim-bullseye
    coverage: "/TOTAL.+ ([0-9]{1,3}%)/"
    artifacts:
        name: $CI_BUILD_REF_NAME
        expire_in: 6 mos
        reports:
            junit: junit.xml
            coverage_report:
              coverage_format: cobertura
              path: coverage.xml

    script:
        - apt-get update
        - apt-get install -y git gcc libc6-dev libmagic1 libpq-dev
        - pip install -r requirements/base.txt
        - pip install -r requirements/test.txt
        - bin/git-hooks/pre-commit -c -j junit.xml

Build container image and push to Gitlab:
    tags: ["mintlab"]
    stage: Build
    rules:
        - if: $CI_MERGE_REQUEST_ID
          when: never
        - if: $CI_COMMIT_TAG =~ /^release\//
        - if: $CI_COMMIT_BRANCH =~ /^(master|production|preprod|development|experimental)$/

    script:
        - /init.sh
        - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
        - docker buildx build
          --platform ${BUILDKIT_ARCHS:-linux/amd64,linux/arm64}
          --target production
          --output type=registry
          --build-arg BUILD_TIMESTAMP="$(date)"
          --build-arg BUILD_REF="${CI_COMMIT_REF_NAME}"
          --tag ${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG}-ci-${CI_PIPELINE_IID}
          --tag ${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG}
          "."

#
# Release targets
#

Copy container image to AWS ECR:
    tags: ["mintlab"]
    stage: Release
    rules:
        - if: $ECR_REPOSITORY_URL == null
          when: never
        - if: $CI_MERGE_REQUEST_ID
          when: never
        - if: $CI_COMMIT_TAG =~ /^release\//

    script:
        - $(aws ecr get-login --no-include-email --region "$AWS_ECR_REGION")
        - skopeo copy --all "docker://${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG}-ci-${CI_PIPELINE_IID}" "docker://${ECR_REPOSITORY_URL}:${CI_COMMIT_REF_SLUG}"
